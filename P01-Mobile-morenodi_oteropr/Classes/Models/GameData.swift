//
//  GameData.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import Foundation
import UIKit

struct GameData {
    
    var name: String
    var image: UIImage
    
    init(name: String, image: UIImage) {
        self.name = name
        self.image = image
    }
    
}


