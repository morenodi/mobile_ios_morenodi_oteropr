//
//  user.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 21/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import Foundation


struct User {
  
  var name: String
  var password: String

  init(){
    self.name = ""
    self.password = ""
  }

  init(name: String, password: String) {
    self.name = name
    self.password = password
  }
  
    
  public static func validateLogin (check_user: User, users: [User]) -> Bool {
    var found = false
    users.forEach { user in
        if (user.name == check_user.name &&
            user.password == check_user.password) {
            print ("OK")
            addUser(user: check_user)
            found = true
        }
    }
    return found
  }
}

func getLastUser() -> User {

    let name = UserDefaults.standard.value(forKey: "User") as? String
    let password = UserDefaults.standard.value(forKey: "Password") as? String
    if(name == nil || password == nil) {
        return User()
    }
    let user = User(name: name!, password: password!)
    print("getLastUser " + user.name + " " + user.password )
    return user
}

func addUser(user : User) -> Void {
    print("addUser " + user.name + " " + user.password )
    UserDefaults.standard.setValue(user.name, forKey: "User")
    UserDefaults.standard.setValue(user.password, forKey: "Password")
}
