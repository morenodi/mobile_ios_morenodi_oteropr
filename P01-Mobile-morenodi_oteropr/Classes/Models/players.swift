//
//  players.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 30/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import Foundation

struct Player {
    var name: String
    var points: Int
    
    init() {
        self.name = ""
        self.points = 0
    }
    
    init(name: String, points: Int) {
        self.name = name
        self.points = points
    }
}
