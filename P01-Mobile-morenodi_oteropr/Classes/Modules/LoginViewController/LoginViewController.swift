//
//  LoginViewController.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 21/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import UIKit
import LoRAPI

class LoginViewController: UIViewController {

    @IBOutlet weak var backLoginView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passInput: UITextField!
    @IBOutlet weak var userInput: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
  
    var myUsers: [User] = []
  
    override func viewDidLoad() {
        super.viewDidLoad()

        let autumn = UIColor(red: 0.46, green: 0.21, blue: 0.15, alpha: 1.0)
        let mist = UIColor(red: 0.56, green: 0.69, blue: 0.77, alpha: 1.0)
        //let stone = UIColor(red: 0.20, green: 0.42, blue: 0.53, alpha: 1.0)
        let shadow = UIColor(red: 0.16, green: 0.19, blue: 0.20, alpha: 1.0)
      
        backLoginView.backgroundColor = mist
        backLoginView.backgroundColor = mist
        backLoginView.layer.cornerRadius = 5
        backLoginView.layer.borderWidth = 1
        backLoginView.layer.borderColor = mist.cgColor
      
        userLabel.textColor = shadow
        passLabel.textColor = shadow
      
        userInput.backgroundColor = shadow
        userInput.textColor = UIColor.white
        passInput.backgroundColor = shadow
        passInput.textColor = UIColor.white
        
        submitButton.tintColor = UIColor.white
        submitButton.backgroundColor = autumn
        submitButton.layer.cornerRadius = 5
        submitButton.layer.borderWidth = 1
        submitButton.layer.borderColor = autumn.cgColor
      
        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = backgroundImageView.bounds
        backgroundImageView.addSubview(blurredEffectView)
      
        // Do any additional setup after loading the view.
        errorLabel.text = ""

        // Clean user defaults
        //addUser(user: User() )
        
        LoRAPI().getURL(url: "https://api.myjson.com/bins/vuz9b") {
          jsonObj in
          self.parseLogin(data: jsonObj)
        }

        let last_user = getLastUser() as User


        if(last_user.name != "" && last_user.password != ""){
          userInput.text = last_user.name
          passInput.text = last_user.password
          navigationController?.pushViewController(GamesViewController(), animated: false)
        }

        submitButton.addTarget(self, action: #selector(self.submitLogin), for: .touchUpInside)
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @objc func submitLogin () {
      print(userInput.text as String?!)
        
      var found = false
      errorLabel.text = ""
        
      found = User.validateLogin(check_user: User(name: userInput.text!, password: passInput.text!), users: self.myUsers)
      if (found) {
        navigationController?.pushViewController(GamesViewController(), animated: false)
      }
      else if (!found) {
        errorLabel.text = "Incorrect Login"
      }
        
        
    }
    
    func parseLogin(data : NSDictionary? ) -> Void {

        if let usersArray = data?.value(forKey: "users") as? NSArray {

            usersArray.forEach { user in

                if let userDict = user as? NSDictionary {
                    if let userName = (userDict.value(forKey: "username") as? String),
                        let userPassword = (userDict.value(forKey: "password") as? String) {

                        myUsers.append(User(
                            name: userName,
                            password: userPassword
                        ))

                    }
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
