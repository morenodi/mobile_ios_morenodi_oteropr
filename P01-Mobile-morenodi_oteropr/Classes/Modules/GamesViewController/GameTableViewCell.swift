//
//  GameTableViewCell.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import UIKit

class GameTableViewCell: UITableViewCell {

    @IBOutlet var gameImageView: UIImageView!
    @IBOutlet var gameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
