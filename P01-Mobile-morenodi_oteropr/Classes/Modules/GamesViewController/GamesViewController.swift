//
//  GamesViewController.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import UIKit
import LoRAPI

class GamesViewController: UIViewController {

    var myGames : [GameData] = []
    
    @IBOutlet var gamesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        gamesTableView.dataSource = self
        gamesTableView.delegate = self
        
        // load cells data
        LoRAPI().getURL(url: "https://api.myjson.com/bins/w7fmz") {
            jsonObj in
            self.parseGames(data: jsonObj)
            DispatchQueue.main.async(execute: {
                self.gamesTableView.reloadData()
            })
        }
        
        // Register custom table view cell
        let unib = UINib(nibName: "GameTableViewCell", bundle: nil)
        self.gamesTableView.register(unib, forCellReuseIdentifier: "GameTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func parseGames(data : NSDictionary? ) -> Void {
        if let gamesArray = data?.value(forKey: "games") as? NSArray {
            gamesArray.forEach { game in
                if let gameDict = game as? NSDictionary {
                    if let gameName = (gameDict.value(forKey: "name") as? String),
                        let gameImage = (gameDict.value(forKey: "image") as? String) {
                        myGames.append(GameData(
                            name: gameName,
                            image: UIImage(named: gameImage)!
                        ))
                        //print(gameName + " " + gameImage)
                    }
                }
            }
        }
    }
    
}

extension GamesViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myGames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = self.gamesTableView.dequeueReusableCell(withIdentifier: "GameTableViewCell") as? GameTableViewCell else {
                return UITableViewCell()
            }
        
        cell.gameLabel.text = myGames[indexPath.row].name
        cell.gameImageView.image = myGames[indexPath.row].image
        
        return cell
    }
    
    
}

extension GamesViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(myGames[indexPath.row].name + " HEY")
        
        let rankingViewController = RankingViewController()
        rankingViewController.gameToUse = myGames[indexPath.row].name
        
        navigationController?.pushViewController(rankingViewController, animated: false)
    }
}

