//
//  RankingTableViewCell.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 30/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import UIKit

class RankingTableViewCell: UITableViewCell {

    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userPointsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
