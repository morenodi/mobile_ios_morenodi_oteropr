//
//  RankingViewController.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 30/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import UIKit
import LoRAPI

class RankingViewController: UIViewController {

    var myPlayers : [Player] = []
    var gameToUse : String = ""
    
    @IBOutlet var rankingTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        rankingTableView.dataSource = self
        rankingTableView.delegate = self
        
        // Load cells data
        LoRAPI().getURL(url: "https://api.myjson.com/bins/dwf3n") {
            jsonObj in
            self.parsePlayers(data: jsonObj, game: self.gameToUse)
            DispatchQueue.main.async(execute: {
                self.rankingTableView.reloadData()
            })
        }
        
        
        // Register custom table view cell
        let unib = UINib(nibName: "RankingTableViewCell", bundle: nil)
        self.rankingTableView.register(unib, forCellReuseIdentifier: "RankingTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func parsePlayers(data : NSDictionary?, game : String ) -> Void {
      if let gamesDict = data?.value(forKey: "players") as? NSDictionary {
        if let gameRankingArray = gamesDict.value(forKey: game) as? NSArray {
            gameRankingArray.forEach { player in
                if let playerDict = player as? NSDictionary {
                    if let playerName = (playerDict.value(forKey: "name") as? String),
                       let playerScore = (playerDict.value(forKey: "point") as? Int) {
                        myPlayers.append(Player(name: playerName,
                                                points: playerScore))
                        print(playerName + " " + String(playerScore))
                    }
                }
            }
        }
      }
    }

    /*func parseGames(data : NSDictionary? ) -> Void {
        if let gamesArray = data?.value(forKey: "games") as? NSArray {
            gamesArray.forEach { game in
                if let gameDict = game as? NSDictionary {
                    if let gameName = (gameDict.value(forKey: "name") as? String),
                        let gameImage = (gameDict.value(forKey: "image") as? String) {
                        myGames.append(GameData(
                            name: gameName,
                            image: UIImage(named: gameImage)!
                        ))
                        //print(gameName + " " + gameImage)
                    }
                }
            }
        }
    }*/
}

extension RankingViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myPlayers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = self.rankingTableView.dequeueReusableCell(withIdentifier: "RankingTableViewCell") as? RankingTableViewCell else {
            return UITableViewCell()
        }
        
        cell.userNameLabel.text = myPlayers[indexPath.row].name
        cell.userPointsLabel.text = String(myPlayers[indexPath.row].points)
        
        return cell
    }
    
    
}

extension RankingViewController : UITableViewDelegate {
    
}
