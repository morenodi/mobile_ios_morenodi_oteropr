//
//  P01_Mobile_morenodi_oteroprTests.swift
//  P01-Mobile-morenodi_oteroprTests
//
//  Created by pvesat on 5/12/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import XCTest

class P01_Mobile_morenodi_oteroprTests: XCTestCase {
    
    var users: [User] = []
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        users.append(User(name: "morenodi", password: "54321"))
        users.append(User(name: "oteropr", password: "12345"))
        users.append(User(name: "test", password: "test"))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        XCTAssert(User.validateLogin(check_user: User(name: "morenodi", password: "54321"), users: self.users))
    }
    
}
