//
//  LoRAPI.swift
//  P01-Mobile-morenodi_oteropr
//
//  Created by pvesat on 21/11/17.
//  Copyright © 2017 pvesatmorenodi_oteropr. All rights reserved.
//

import Foundation

public final class LoRAPI {
    
  public init () {
        
  }
    
  public func getURL (url: String, completitionHandler: @escaping (NSDictionary?) -> Void) {
    guard let json_url = URL(string: url) else {
      return
   }
    
    let dataTask = URLSession.shared.dataTask(with: json_url, completionHandler: { data, response, error -> Void in
                
      guard let data = data,
        let jsonObj =  try? JSONSerialization.jsonObject(with: data,
          options: .allowFragments) as? NSDictionary else {
            
        return
      }

        completitionHandler(jsonObj)
                            
    })
      
    dataTask.resume()
  }
  
}
