
Pod::Spec.new do |s|
  s.name             = 'LoRAPI'
  s.version          = '0.1.0'
  s.summary          = 'Multithreading json reader'
 
  s.description      = <<-DESC
Multithreading json reader
                       DESC
 
  s.homepage         = ':D'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.authors          = { 'morenodi' => 'morenodi@esat-alumni.com' }, { 'otero' => 'oteropr@esat-alumni.com' }
  s.source           = { :git => ':D', :tag => s.version.to_s }
 
  s.ios.deployment_target = '9.3'
  s.source_files = 'Classes/LoRAPI.swift'

end